# Description

A new Flutter project.

This project is the first app from [Learn Flutter & Dart to Build iOS & Android Apps](https://www.udemy.com/course/learn-flutter-dart-to-build-ios-android-apps/) course on [Udemy](https://www.udemy.com).

Thanks to
[Maximilian Schwarzmüller]
(https://www.udemy.com/course/learn-flutter-dart-to-build-ios-android-apps/#instructor-1)
